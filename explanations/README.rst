.. -*- restructuredtext -*-

=======================
p0ss1ble — explanations
=======================

* Sur fond vert : singleton (ensemble ne contenant qu'une seule valeur "possible").
* Sur fond bleu : valeur correspond à un caractère non affichageable (autre qu'une fin de ligne).
* Sinon, sur fond blanc à fond noir : caractère le plus "probable" parmi un ensemble de valeurs "possibles" non unique (plus le nombre de valeurs est grand et plus le fond est foncé).

* Souligné en orange : caractère commun à un autre de la même colonne.

Info-bulle :

    | ``+---------------------------------------------------------------------------------------------------------+``
    | ``| (indice de ligne, indice de colonne) #taille de l'ensemble: valeurs "possibles" sous forme de caractère |``
    | ``| valeurs "possibles"                                                                                     |``
    | ``| Crypted: valeur du message crypté                                                                       |``
    | ``|                                                                                                         |``
    | ``| [Same than message(s): indice des lignes des caractères communs de la même colonne]                     |``
    | ``+---------------------------------------------------------------------------------------------------------+``

Dans l'exemple result_popup.png le caractère colonne 4, ligne 45 est pointé :

    | ``+------------------------------+``
    | ``| (4, 45) #6: eijkmo           |``
    | ``| 101, 105, 106, 107, 109, 111 |``
    | ``| Crypted: 116 = 0x74          |``
    | ``+------------------------------+``

Le "e" est proposé car plus probable que les autres (par la fréquence des lettres en anglais).
Mais en fait le caractère correct est le "i" qui est aussi dans les valeurs "possibles".
C'est le "i" du mot "while".



L'exemple d'utlisation du mode interactif dans interactive_mode.png fixe les valeurs
à partir de (4, 43) sur les valeurs "while".
