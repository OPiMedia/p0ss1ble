/*
  p0ss1ble.py (January 31, 2017)
  https://bitbucket.org/OPiMedia/p0ss1ble

  GPLv3 --- Copyright (C) 2016, 2017 Olivier Pirson
  http://www.opimedia.be/
*/

(function () {
    "use strict";

    function killPopup() {
        const divs = document.getElementsByClassName("msg");

        for (let div of divs) {
            const spans = div.children;

            for (let span of spans) {
                const inner_spans = span.children;

                if (inner_spans) {
                    for (let inner_span of inner_spans) {
                        span.removeChild(inner_span);
                    }
                }
            }
        }
    }

    window.addEventListener("load", function () {
        document.getElementById("killPopupButton").addEventListener("click", killPopup);
    });
}());
