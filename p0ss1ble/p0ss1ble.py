#!/usr/bin/env python3
# -*- coding: latin-1 -*-

"""
p0ss1ble.py (January 31, 2017)
https://bitbucket.org/OPiMedia/p0ss1ble

"Interactive" program that produces a HTML output
to decrypt a list of ciphertexts crypted with same "xor" key.

./p0ss1ble.py [messages directory]

  - message directory: by default "../assignment/team03/"

Run and tested on Python 3.4.2 (and PyPy Python 3.2.5).

GPLv3 --- Copyright (C) 2016, 2017 Olivier Pirson
http://www.opimedia.be/
"""

from __future__ import division
from __future__ import print_function

import base64
import collections
import glob
import html
import os
import sys


#
# Global constants
##################
INIT_KEY_VALUES = tuple(range(256))
INIT_PLAIN_CHARS = tuple(sorted('\n .abcdefghijklmnopqrstuvwxyz'))
INIT_PLAIN_VALUES = tuple(ord(c) for c in INIT_PLAIN_CHARS)
INIT_PLAIN_VALUES_SET = set(INIT_PLAIN_VALUES)

IGNORED_MSGS = (7, )  # some functions ignore (like Russian message)

FIGURES = '0123456789'
MAJUSCULES = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
MINUSCULES = 'abcdefghijklmnopqrstuvwxyz'

assert len(set(FIGURES)) == 10
assert len(set(MAJUSCULES)) == 26
assert MINUSCULES == MAJUSCULES.lower()

ALPHANUM = FIGURES + MAJUSCULES + MINUSCULES

POSSIBLE_REVERSE_CACHE = dict()

FILENAME_KEY = 'results/key.txt'
DIRECTORY_HTML = 'html/'
FILENAME_HTML = DIRECTORY_HTML + '/result.html'


#
# Global variable
#################
MESSAGES_DIRECTORY = '../assignment/team03/'


#
# Classes
#########
class Char:
    """
    A character on one byte
    represented by its crypted value
                   and all its plain *possible* values.
    """

    def __init__(self, crypted):
        """
        Init with crypted
        and the set of plain values from INIT_PLAIN_VALUES.

        :param crypted: 0 <= int < 256
        """
        assert is_value(crypted), (type(crypted), crypted)

        self.__crypted = crypted

        self.__plains = set(INIT_PLAIN_VALUES)
        self.__same_chars = set()

    def __repr__(self):
        return 'Char({})'.format(self.get_crypted())

    def __str__(self):
        plains = self.get_plains()

        if len(plains) == 0:
            return 'X'
        elif len(plains) == 1:
            return value_to_printable(top(plains))
        else:
            return '?'

    def add_same_char(self, ith):
        assert isinstance(ith, int), type(ith)
        assert ith >= 0, ith

        self.__same_chars.add(ith)

    def get_crypted(self):
        """
        Return the crypted value.

        :return: 0 <= int < 256
        """
        return self.__crypted

    def get_same_chars(self):
        """
        Return the set of indices of all message
        that have the same character in the same column.

        :warning: the set is *not* copied

        :return: set of (int >= 0)
        """

        return self.__same_chars

    def get_plains(self):
        """
        Return the set of all possible plain values.

        :warning: the set is *not* copied

        :return: set of (0 <= int < 256)
        """
        return self.__plains

    def intersection_plains_if(self, other_plains):
        """
        Modified the set of plain values
        by taking intersection with `other_plains`
        if this intersection is not null.

        :param other_plains: Char
        """
        assert is_values_set(other_plains), (type(other_plains), other_plains)

        if self.__plains & other_plains:
            self.__plains &= other_plains

    def is_unique(self):
        return len(self.__plains) == 1

    def set_plains(self, plains):
        assert is_values_set(plains), (type(plains), plains)

        self.__plains = plains

    def to_html(self, i, j, print_crypted=True):
        assert isinstance(i, int), type(i)
        assert i >= 0, i

        assert isinstance(j, int), type(j)
        assert j >= 0, j

        assert isinstance(print_crypted, bool), type(print_crypted)

        plains = self.get_plains()

        char = None
        html_class = ''
        style = ''

        if len(plains) == 0:
            html_class = 'empty'
        elif len(plains) == 1:
            plain = top(self.get_plains())
            char = chr(plain)
            if plain in (10, 13):
                html_class = 'eol'
            elif not char.isprintable():
                char = None
                html_class = 'unprintable'
        else:
            char = guess_alphanum(plains)
            html_class = 'unknow'
            style = ('background-color: rgb({0}, {0}, {0});'
                     .format(256 - len(plains)))

        crypted = """Crypted: {0} = 0x{0:X}<br>
""".format(self.get_crypted()) if print_crypted else ''

        popup = """({}, {})
#{}: {}<br>
{}<br>{}""".format(i, j,
                   len(plains),
                   ''.join(html.escape(value_to_printable(plain, ' '))
                           for plain in sorted(plains)),
                   ', '.join(str(plain) for plain in sorted(plains)),
                   crypted)
        if len(self.get_same_chars()) > 1:
            popup += ("""<br>
Same than message(s): {}"""
                      .format(', '.join(str(i)
                                        for i in self.get_same_chars())))
            html_class += ' same'
        popup = """
<span>{}</span>""".format(popup)

        if html_class != '':
            html_class = ' class="{}"'.format(html_class)
        if style != '':
            style = ' style="{}"'.format(style)

        char = (html.escape(char)
                .replace('\n', '&crarr;').replace('\r', '&crarr;')
                .replace(' ', '&nbsp;')
                if char is not None
                else '&nbsp;')

        return '<span{}{}>{}{}</span>'.format(html_class, style,
                                              char, popup)


class KeyValue:
    """
    A value on one byte
    represented by all its *possible* values.
    """

    def __init__(self):
        """
        Init with the set of values.
        """
        self.__values = set(INIT_KEY_VALUES)

    def get_values(self):
        return self.__values

    def intersection_values_if(self, other_values):
        """
        Modified the set of values
        by taking intersection with `other_values`
        if this intersection is not null.

        :param other_values: KeyValue
        """
        assert is_values_set(other_values), (type(other_values), other_values)

        if self.__values & other_values:
            self.__values &= other_values

    def is_unique(self):
        return len(self.__values) == 1

    def set_values(self, values):
        assert is_values_set(values), (type(values), values)

        self.__values = values

    def to_html(self, i, j):
        assert isinstance(i, int), type(i)
        assert i >= -1, i

        assert isinstance(j, int), type(j)
        assert j >= 0, j

        char = Char(0)
        char.set_plains(self.get_values())

        return char.to_html(0, j, False)


#
# Functions
###########
def cmd_split(cmd, pattern):
    assert isinstance(cmd, str), cmd
    assert len(cmd) > 0

    assert isinstance(pattern, str), pattern
    assert len(pattern) > 0

    params = cmd.split()
    if len(params) != len(pattern):
        print('! {} parameters expected, {} received'
              .format(len(pattern) - 1, len(params) - 1))

        return (None, )*len(pattern)

    res = []

    for i, param in enumerate(params):
        if pattern[i] == 'C':    # command
            res.append(param)
        elif pattern[i] == 'c':  # character
            if len(param) != 1:
                res[0] = None

                break

            res.append(param)
        elif pattern[i] == 'n':  # integer
            try:
                param = int(param)
            except ValueError:
                param = -1

            if param < 0:
                res[0] = None

                break

            res.append(param)
        elif pattern[i] == 's':  # string
            res.append(param)

    if not res:
        print('! Syntax error')

        return (None, )*len(pattern)
    elif res[0] is None:
        print('! Value error')

        return (None, )*len(pattern)

    return tuple(res)


def connect_same_char(key, messages, ith, jth):
    """
    Set a unique set of plains
    for each chars on the same columns that are the same crypted value.
    """
    assert isinstance(key, tuple), type(key)
    assert isinstance(messages, tuple), type(messages)

    assert isinstance(ith, int), type(ith)
    assert 0 <= ith < len(messages), ith

    assert isinstance(jth, int), type(jth)
    assert 0 <= jth < len(messages[ith]), jth

    char = messages[ith][jth]
    crypted = char.get_crypted()

    for i, message in enumerate(messages):
        if (i < ith) and (jth < len(message)):
            char_i = message[jth]
            if crypted == char_i.get_crypted():
                char.add_same_char(i)
                char_i.add_same_char(ith)


def filename_to_result_filename(filename):
    assert isinstance(filename, str), type(filename)

    return 'results/' + os.path.basename(os.path.splitext(filename)[0])


def for_each_char(key, messages, fct):
    assert isinstance(key, tuple), type(key)
    assert isinstance(messages, tuple), type(messages)
    assert isinstance(fct, collections.Callable), type(fct)

    for j in range(max_length(messages)):
        for_each_char_of_column(key, messages, j, fct)


def for_each_char_of_column(key, messages, jth, fct):
    assert isinstance(key, tuple), type(key)
    assert isinstance(messages, tuple), type(messages)
    assert isinstance(jth, int), type(jth)
    assert isinstance(fct, collections.Callable), type(fct)

    for i, message in enumerate(messages):
        if jth < len(message):
            fct(key, messages, i, jth)


def for_each_keyvalue(key, messages, fct):
    assert isinstance(key, tuple), type(key)
    assert isinstance(messages, tuple), type(messages)
    assert isinstance(fct, collections.Callable), type(fct)

    for j in range(max_length(messages)):
        fct(key, messages, j)


def guess_alphanum(plains):
    assert is_values_set(plains), (type(plains), plains)

    chars = tuple(frozenset(chr(value) for value in plains)
                  & frozenset(ALPHANUM))

    char = None
    if len(chars) == 1:  # unique alphanum
        char = chars[0]
    else:
        chars = tuple(frozenset(''.join(chr(value)
                                        for value in plains))
                      & (frozenset(MAJUSCULES) | frozenset(MINUSCULES)))
        if len(chars) == 1:  # unique letter
            char = chars[0]
        else:
            chars = tuple(frozenset(''.join(chr(value)
                                            for value in plains).lower())
                          & (frozenset(MINUSCULES)))
            if len(chars) == 1:  # unique minuscule
                char = chars[0]
            else:
                minuscules = tuple('etaoinsrhldcumfpgwybvkxjqz')
                # http://www.bckelk.ukfsn.org/words/etaoin.html

                plains = tuple(plains)
                for minuscule in minuscules:
                    try:
                        i = tuple(chr(plain).lower()
                                  for plain in plains).index(minuscule)

                        return chr(plains[i])
                    except ValueError:
                        pass

                char = None

    return char


def interactive_mode(out_filename, key, filenames, messages):
    assert isinstance(out_filename, str), type(out_filename)
    assert isinstance(key, tuple), type(key)
    assert isinstance(filenames, tuple), type(filenames)
    assert isinstance(messages, tuple), type(messages)

    first_column = 0

    while True:
        print("""
Available commands:
  s i j v         set unique value of (i, j) to value v and solve column
  S i j s         set unique value of (i, j) and next to string s and solve columns

  first j         set j as the first column write in HTML output (= {0})
  u               update (write) HTML outputs in "{1}"

  read f x y      load content of file f and set to (x, y) and next and solve columns
  readkey         read key in "{2}" (0 are ignored)
  readkey force   read key in "{2}" (force 0)
  write           write solutions "{3}" (and key)
  writekey        write key in "{2}"

  quit            quit""".format(first_column, DIRECTORY_HTML, FILENAME_KEY,
                                 filename_to_result_filename('')))
        cmd = input('>>> ')

        if cmd[:6] == 'first ':
            cmd, j = cmd_split(cmd, 'Cn')
            if cmd is not None:
                first_column = j
        elif cmd == 'quit':
            break
        elif cmd[:5] == 'read ':  # read f i j
            cmd, filename, ith, jth = cmd_split(cmd, 'Csnn')
            if (cmd is not None) and (ith < len(messages)):
                try:
                    with open(filename, 'rb') as fin:
                        plains = fin.read()

                    for j, plain in enumerate(plains):
                        if jth + j >= len(messages[ith]):
                            break

                        messages[ith][jth + j].set_plains(to_set(plain))
                        solve_column_from_unique_char(key, messages,
                                                      ith, jth + j)
                except FileNotFoundError:
                    print('! File not found')
        elif cmd == 'readkey':
            read_key(key)
            for_each_char(key, messages, solve_char_from_unique_key)
        elif cmd == 'readkey force':
            read_key(key, force=True)
            for_each_char(key, messages, solve_char_from_unique_key)
        elif cmd[:2].lower() == 's ':  # s i j v or S i j c
            if cmd[0] == 's':
                cmd, ith, jth, plain = cmd_split(cmd, 'Cnnn')
                plains = (plain, )
            else:
                cmd, ith, jth, string = cmd_split(cmd, 'Cnns')
                if cmd is not None:
                    plains = tuple(ord(char) for char in string)

            if (cmd is not None) and (ith < len(messages)):
                for j, plain in enumerate(plains):
                    if jth + j >= len(messages[ith]):
                        break

                    messages[ith][jth + j].set_plains(to_set(plain))
                    solve_column_from_unique_char(key, messages, ith, jth + j)
        elif cmd == 'u':
            write_html(out_filename, key, filenames, messages,
                       first_column=first_column)
        elif cmd == 'write':
            write_solutions(filenames, key, messages)
        elif cmd == 'writekey':
            write_key(key)
        else:
            print('! Unknow command')


def is_same_prev_char(messages, ith, jth):
    assert isinstance(messages, tuple), type(messages)

    assert isinstance(ith, int), type(ith)
    assert 0 <= ith < len(messages), ith

    assert isinstance(jth, int), type(jth)
    assert 0 <= jth < len(messages[ith]), jth

    for i_message in messages[ith][jth].get_same_chars():
        assert i_message != ith

        if i_message < ith:
            return True

    return False


def init_key(messages):
    assert isinstance(messages, tuple), type(messages)

    return tuple(KeyValue() for _ in range(max_length(messages)))


def is_char(item):
    """
    Returns True if item is string of length 1,
    else False.
    """
    return isinstance(item, str) and (len(item) == 1)


def is_value(item):
    return isinstance(item, int) and (0 <= item < 256)


def is_values_set(item):
    if not (isinstance(item, set) or isinstance(item, frozenset)):
        return False

    for i in item:
        if not is_value(i):
            return False

    return True


def iter_column(messages, ith):
    assert isinstance(messages, tuple), type(messages)

    assert isinstance(ith, int), type(ith)
    assert 0 <= ith < len(messages), ith

    for message in messages:
        assert isinstance(message, tuple), type(message)

        yield (message[ith] if ith < len(message)
               else None)


def max_length(messages):
    assert isinstance(messages, tuple), type(messages)

    return max(len(message) for message in messages)


def possible_reverse_xor(xored_value):
    assert is_value(xored_value), (type(xored_value), xored_value)

    values = POSSIBLE_REVERSE_CACHE.get(xored_value)
    if values is None:
        values = set()

        for i, i_plain in enumerate(INIT_PLAIN_VALUES):
            for j in range(i):
                j_plain = INIT_PLAIN_VALUES[j]
                if i_plain ^ j_plain == xored_value:
                    values.add(i_plain)
                    values.add(j_plain)

        values = frozenset(values)
        POSSIBLE_REVERSE_CACHE[xored_value] = values

    return values


def read_crypted_message(filename):
    assert isinstance(filename, str), type(filename)

    message = tuple()

    with open(filename) as fin:
        for line in fin:
            line = line.rstrip('\n')

            line = base64.standard_b64decode(bytes(line, encoding='ascii'))
            line = tuple(Char(value) for value in line)

            message += line

    return message  # [:50]  # usefull to development


def read_crypted_messages(directory):
    assert isinstance(directory, str), type(directory)

    filenames = tuple(sorted(glob.glob(directory + '/*.enc')))
    messages = tuple(read_crypted_message(filename) for filename in filenames)

    return filenames, messages


def read_key(key, force=False):
    assert isinstance(key, tuple), type(key)
    assert isinstance(force, bool), type(force)

    filename = FILENAME_KEY
    print('Loads key to "{}"'.format(filename))

    with open(filename, 'rb') as fout:
        data = fout.read()

    for i, keyvalue in enumerate(key):
        if (i < len(data)) and (force or (data[i] != 0)):
            keyvalue.set_values(to_set(data[i]))


def solve_char_by_possible_xor(key, messages, ith, jth):
    """
    For each other message than ith,
    calculates possible values that will give crypted ^ other_crypted
    and if this set of values intersects with plains
    |   then set intersection for the ith message and other message.
    """
    assert isinstance(key, tuple), type(key)
    assert isinstance(messages, tuple), type(messages)

    assert isinstance(ith, int), type(ith)
    assert 0 <= ith < len(messages), ith

    assert isinstance(jth, int), type(jth)
    assert 0 <= jth < len(messages[ith]), jth

    char = messages[ith][jth]
    crypted = char.get_crypted()

    for i, message in enumerate(messages):
        if ((i == ith) or (jth >= len(message))
                or is_same_prev_char(messages, ith, jth)):
            continue

        char_i = message[jth]
        xored = crypted ^ char_i.get_crypted()
        values = possible_reverse_xor(xored)
        if values:
            char.intersection_plains_if(values)
            char_i.intersection_plains_if(values)


def solve_char_from_unique_key(key, messages, ith, jth):
    """
    If the key have only one plain value
    then set char plain.
    """
    assert isinstance(key, tuple), type(key)
    assert isinstance(messages, tuple), type(messages)

    assert isinstance(ith, int), type(ith)
    assert 0 <= ith < len(messages), ith

    assert isinstance(jth, int), type(jth)
    assert 0 <= jth < len(messages[ith]), jth

    values = key[jth].get_values()
    message = messages[ith]

    if (jth < len(message)) and (len(values) == 1):
        # FIXME? and not is_same_prev_char(messages, ith, jth)):
        value = top(values)
        char = message[jth]
        crypted = char.get_crypted()
        plain = crypted ^ value
        char.set_plains(to_set(plain))


def solve_column_from_unique_char(key, messages, ith, jth):
    """
    If char has unique plain value
    then deduce key value and plain values of all this column.
    """
    assert isinstance(key, tuple), type(key)
    assert isinstance(messages, tuple), type(messages)

    assert isinstance(ith, int), type(ith)
    assert 0 <= ith < len(messages), ith

    assert isinstance(jth, int), type(jth)
    assert 0 <= jth < len(messages[ith]), jth

    char = messages[ith][jth]
    plains = char.get_plains()
    if len(plains) != 1:
        return

    plain = top(plains)
    xored = char.get_crypted() ^ plain
    key[jth].set_values(to_set(xored))

    for_each_char_of_column(key, messages, jth, solve_char_from_unique_key)


def solve_keyvalue_by_most(key, messages, jth):
    """
    If key at several values
    then try each of them to each crypted message (excepted IGNORED_MSGS)
    |    and if one of these value if better than other
    |        then set it.
    """
    assert isinstance(key, tuple), type(key)
    assert isinstance(messages, tuple), type(messages)

    assert isinstance(jth, int), type(jth)
    assert 0 <= jth < max_length(messages), jth

    keyvalue = key[jth]
    values = keyvalue.get_values()

    if len(values) <= 1:
        return

    histo = {}

    for value in values:
        nb_good = 0
        for i, message in enumerate(messages):
            if ((jth >= len(message)) or (i in IGNORED_MSGS)
                    or is_same_prev_char(messages, i, jth)):
                continue

            char = message[jth]
            crypted = char.get_crypted()
            plain = crypted ^ value
            if plain in INIT_PLAIN_VALUES_SET:
                nb_good += 1

        histo[value] = nb_good

    bests = tuple(sorted(((nb_good, value)
                          for value, nb_good in histo.items()),
                         reverse=True))

    if bests[0][0] > bests[1][0]:
        keyvalue.set_values(to_set(bests[0][1]))


def solve_keyvalue_by_possible_chars(key, messages, jth):
    """
    Set key values
    deduced from intersection of "good" values giving by plain values
    (excepted IGNORED_MSGS).
    """
    assert isinstance(key, tuple), type(key)
    assert isinstance(messages, tuple), type(messages)

    assert isinstance(jth, int), type(jth)
    assert 0 <= jth < max_length(messages), jth

    keyvalue = key[jth]

    values = set()
    for i, message in enumerate(messages):
        if ((jth >= len(message)) or (i in IGNORED_MSGS)
                or is_same_prev_char(messages, i, jth)):
            continue

        char = message[jth]
        plains = char.get_plains()
        if len(plains) == len(INIT_PLAIN_VALUES):
            continue

        for plain in plains:
            xored = char.get_crypted() ^ plain
            values.add(xored)

    if keyvalue.get_values() & values:
        keyvalue.intersection_values_if(values)


def top(iterable):
    """
    :param s: not empty Iterable and Sized

    :return: item of set
    """
    assert isinstance(iterable, collections.Iterable), type(iterable)
    assert isinstance(iterable, collections.Sized), type(iterable)
    assert len(iterable) > 0

    return next(iter(iterable))


def to_set(item):
    return set((item, ))


def value_to_printable(value, default_char='!'):
    assert isinstance(value, int), type(value)
    assert 0 <= value < 256, value

    assert is_char(default_char), (type(default_char), default_char)

    char = chr(value)

    return (char if char.isprintable()
            else default_char)


def write_html(out_filename, key, filenames, messages, first_column=0):
    assert isinstance(out_filename, str), type(out_filename)
    assert isinstance(key, tuple), type(key)
    assert isinstance(filenames, tuple), type(filenames)
    assert isinstance(messages, tuple), type(messages)

    assert isinstance(first_column, int), type(first_column)
    assert first_column >= 0, first_column

    fout = open(out_filename, 'w')

    write_html_header(fout)

    write_html_results(fout, key, filenames, messages,
                       first_column=first_column)
    write_html_unicode_messages(fout, messages)

    write_html_footer(fout)

    fout.close()


def write_html_footer(fout):
    print("""    </main>
    Useful resources:
    <ul>
      <li>Complete sources of <a class="monospace" href="https://bitbucket.org/OPiMedia/p0ss1ble">p0ss1ble</a> on Bitbucket</li>
      <li><button id="killPopupButton">Kill pop-ups</button></li>
      <li><a href="https://fr.wikipedia.org/wiki/American_Standard_Code_for_Information_Interchange#Table_des_128_caract.C3.A8res_ASCII">ASCII</a></li>
      <li>Firefox plugin: <a href="https://addons.mozilla.org/en-US/firefox/addon/reloadevery/">ReloadEvery</a></li>
      <li><a href="http://www.bckelk.ukfsn.org/words/etaoin.html">Letter frequencies (rankings for various languages)</a></li>
      <li><a href="http://www.opimedia.be/DS/online-tools/txt2/">txt2</a></li>
      <li><a href="http://www.opimedia.be/DS/mementos/uchars10180.htm">uchars10180</a></li>
    </ul>
  </body>
</html>""", file=fout)
    fout.flush()


def write_html_header(fout):
    print("""<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>p0ss1ble &mdash; {}</title>
    <link rel="stylesheet" type="text/css" href="p0ss1ble.css">
    <script type="text/javascript" src="p0ss1ble.js" async></script>
  </head>

  <body>
    <main>
""".format(MESSAGES_DIRECTORY), file=fout)
    fout.flush()


def write_html_results(fout, key, filenames, messages, first_column=0):
    assert isinstance(key, tuple), type(key)
    assert isinstance(filenames, tuple), type(filenames)
    assert isinstance(messages, tuple), type(messages)

    assert isinstance(first_column, int), type(first_column)
    assert first_column >= 0, first_column

    # Key
    print("""<div class="msg">
  <span class="key"><a href="../{0}">k</a></span>
  <span class="found">{1}<span class="">&quot;Found&quot; {1}/{2}</span></span>"""
          .format(FILENAME_KEY,
                  len(tuple(charkey for charkey in key
                            if charkey.is_unique())),
                  len(key)),
          file=fout)

    for j in range(first_column, len(key)):
        keyvalue = key[j]
        print(keyvalue.to_html(-1, j), end='', file=fout)
    print("""
</div>""",
          file=fout)
    fout.flush()

    # Indices
    print("""<div class="msg">
  <span></span>
  <span class="found"></span>""",
          file=fout)
    for j in range(first_column, max_length(messages)):
        print('<span class="position">{}<span>{}</span></span>'
              .format(j % 10, j),
              end='', file=fout)
    print("""
</div>""", file=fout)
    fout.flush()

    # Messages
    for i, message in enumerate(messages):
        message = message[first_column:]

        print("""<div class="msg">
  <span class="filename"><a href="../{0}">{1}</a><span>{2}</span></span>
  <span class="found">{3}<span class="">Found {3}/{4}</span></span>"""
              .format(filename_to_result_filename(filenames[i]),
                      i % 10, html.escape(filenames[i]),
                      len(tuple(char for char in message
                                if char.is_unique())),
                      len(message)),
              file=fout)

        for j, char in enumerate(message):
            print(char.to_html(first_column + i, j), end='', file=fout)

        print('</div>', file=fout)
        fout.flush()


def write_html_unicode_message(fout, i, message):
    assert isinstance(i, int), type(i)
    assert i >= 0, i

    assert isinstance(message, tuple), type(message)

    bytes_msg = bytes((top(char.get_plains())
                       if len(char.get_plains()) == 1
                       else 32)
                      for char in message)
    unicode_msg = str(bytes_msg, encoding='utf_8', errors='replace')
    print('<div class="unicode">', file=fout)
    print('  <div>{}:</div>'.format(i), file=fout)
    print(html.escape(unicode_msg)
          .replace('\n', '<br>\n').replace('\r', '<br>\r'), file=fout)
    print('</div>', file=fout)
    fout.flush()


def write_html_unicode_messages(fout, messages):
    assert isinstance(messages, tuple), type(messages)

    for i, message in enumerate(messages):
        write_html_unicode_message(fout, i, message)


def write_key(key):
    assert isinstance(key, tuple), type(key)

    filename = FILENAME_KEY
    print('Writes key to "{}"'.format(filename))

    data = []
    for k in key:
        keyvalues = k.get_values()
        data.append(top(keyvalues) if len(keyvalues) == 1
                    else 0)

    byte_data = bytearray(data)
    with open(filename, 'wb') as fout:
        fout.write(byte_data)


def write_solution(filename, i, message):
    assert isinstance(filename, str), type(filename)

    assert isinstance(i, int), type(i)
    assert 0 <= i < len(filename), (i, len(filename))

    assert isinstance(message, tuple), type(message)

    filename = filename_to_result_filename(filename)
    print('Writes solution {} to "{}"'.format(i, filename))

    message = bytes((top(char.get_plains())
                     if len(char.get_plains()) == 1
                     else 32)
                    for char in message)

    with open(filename, 'wb') as fout:
        fout.write(message)


def write_solutions(filenames, key, messages):
    assert isinstance(filenames, tuple), type(filenames)
    assert isinstance(key, tuple), type(key)
    assert isinstance(messages, tuple), type(messages)

    write_key(key)

    for i, filename in enumerate(filenames):
        write_solution(filename, i, messages[i])


#
# Main
######
def main():
    """
    Main
    """
    # Params
    if len(sys.argv) > 1:
        global MESSAGES_DIRECTORY

        MESSAGES_DIRECTORY = sys.argv[1]

    # Read messages and init key
    filenames, messages = read_crypted_messages(MESSAGES_DIRECTORY)

    print("Messages loaded, I'm solving it. Wait few seconds...")
    sys.stdout.flush()

    key = init_key(messages)

    # Set same set of values to chars that are same crypted value.
    for_each_char(key, messages, connect_same_char)

    write_html(DIRECTORY_HTML + 'result.0.html', key, filenames, messages)

    # Solve
    for_each_char(key, messages, solve_char_by_possible_xor)

    write_html(DIRECTORY_HTML + 'result.1.html', key, filenames, messages)

    for_each_keyvalue(key, messages, solve_keyvalue_by_possible_chars)
    for_each_char(key, messages, solve_char_from_unique_key)

    write_html(DIRECTORY_HTML + 'result.2.html', key, filenames, messages)

    for_each_keyvalue(key, messages, solve_keyvalue_by_most)
    for_each_char(key, messages, solve_char_from_unique_key)

    # Write final HTML
    out_filename = FILENAME_HTML
    write_html(out_filename, key, filenames, messages)

    # Interactive mode
    interactive_mode(out_filename, key, filenames, messages)

if __name__ == '__main__':
    main()
