.. -*- restructuredtext -*-

====================
p0ss1ble — assigment
====================
Assigment by `Nikita Veshchikov`_,
for the course `INFO-F405 Introduction to cryptography`_
(`Olivier Markowitch`_, Master in Computer science, ULB 2016)

"The purpose of this project is to break encryption in some cases when a strong algorithm is misused.
We are going to use an example of AES-128 CTR."

.. _`INFO-F405 Introduction to cryptography`: http://banssbfr.ulb.ac.be/PROD_frFR/bzscrse.p_disp_course_detail?cat_term_in=201617&subj_code_in=INFO&crse_numb_in=F405&PPAGE=ESC_PROGCAT_AREREQ&PPROGCODE=MA-INFO&PAREA=M-INFOS&PARETERM=201617&PTERM=201617
.. _`Nikita Veshchikov`: http://student.ulb.ac.be/~nveshchi/
.. _`Olivier Markowitch`: http://www.ulb.ac.be/di/scsi/markowitch/

With solutions given by p0ss1ble.
