(function () {
    "use strict";

    function killPopup() {
        var divs = document.getElementsByClassName("ConnectedUnknowChars");
        var spans;
        var inner_spans;
        var i;
        var j;

        for (i in divs) {
            spans = divs[i].children;
            for (i in spans) {
                inner_spans = spans[i].children;
                if (inner_spans) {
                    for (j = 0; j < inner_spans.length; ++j) {
                        //console.log(inner_spans[i]);
                        // inner_spans[j].innerHTML = null;
                        spans[i].removeChild(inner_spans[j]);
                        // spans[i].removeChild(spans[i].children[0]);
                        //spans[i].innerHTML = null;
                    }
                }
            }
        }
    }

    window.addEventListener("load", function () {
        document.getElementById("killPopupButton").addEventListener("click", killPopup);
    });
}());
