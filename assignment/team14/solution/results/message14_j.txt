I heard that you're settled down
Open mind for a different view
You made a really deep cut
It's such a feelin' that my love
You can't always get what you want
But with you, it's not like that at all
If I'm not back again this time tomorrow
Take me to the magic of the moment
Already brushing off the dust