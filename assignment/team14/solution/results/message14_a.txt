Riga is one of the biggest cities in Latvia, it is also its capital.
Almost 700 thousand people live in Riga. The city lies on the area of 304 square kilometers.
Riga is situated on the Daugava river, right in the middle of the Gulf of Riga.
The city is in the UTC+2 time zone.
Visitors foten go there to see the Freedom Monument, Līvu Square, 
the House of the Blackheads and the Latvian National Opera.
You should certainly swim in the sea when you visit Riga
also, do not forget to go and watch a play at a local theater!