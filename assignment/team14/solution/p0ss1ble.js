(function () {
    "use strict";

    function killPopup() {
        const divs = document.getElementsByClassName("msg");

        for (let div of divs) {
            const spans = div.children;

            for (let span of spans) {
                const inner_spans = span.children;

                if (inner_spans) {
                    for (let inner_span of inner_spans) {
                        span.removeChild(inner_span);
                    }
                }
            }
        }
    }

    window.addEventListener("load", function () {
        document.getElementById("killPopupButton").addEventListener("click", killPopup);
    });
}());
