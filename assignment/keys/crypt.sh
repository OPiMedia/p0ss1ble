#!/bin/bash

IV="DEADBEEFCAFEBABEDEADBEEFCAFEBABE"

for g in {1..14} ; do
    echo "GROUP $g";
    m=$(printf "message%s_*.txt" "$g") ;
    for f in `ls $m`; do
        echo $f ;
        openssl enc -aes-128-ctr -kfile team$g.key -nosalt -iv $IV -in $f -a -out $f.enc ;
    done;
done;
