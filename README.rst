.. -*- restructuredtext -*-

========
p0ss1ble
========
Interactive Python program that produces a HTML output
to **decrypt a list of ciphertexts crypted with same "xor" key**.


|output|
|interactive|

.. |output| image:: https://bitbucket.org/OPiMedia/p0ss1ble/raw/master/explanations/result_popup.png
.. |interactive| image:: https://bitbucket.org/OPiMedia/p0ss1ble/raw/master/explanations/interactive_mode.png


HTML output example:

0. `step 0`_
1. `step 1`_
2. `step 2`_
3. `step 3`_
4. `step decrypted`_

.. _`step 0`: http://www.opimedia.be/CV/2016-2017-ULB/INFO-F405-Introduction-to-cryptography/Project-3-Misuse-of-encryption/after-step-0/p0ss1ble/result.html
.. _`step 1`: http://www.opimedia.be/CV/2016-2017-ULB/INFO-F405-Introduction-to-cryptography/Project-3-Misuse-of-encryption/after-step-1/p0ss1ble/result.html
.. _`step 2`: http://www.opimedia.be/CV/2016-2017-ULB/INFO-F405-Introduction-to-cryptography/Project-3-Misuse-of-encryption/after-step-2/p0ss1ble/result.html
.. _`step 3`: http://www.opimedia.be/CV/2016-2017-ULB/INFO-F405-Introduction-to-cryptography/Project-3-Misuse-of-encryption/after-step-3/p0ss1ble/result.html
.. _`step decrypted`: http://www.opimedia.be/CV/2016-2017-ULB/INFO-F405-Introduction-to-cryptography/Project-3-Misuse-of-encryption/decrypted/p0ss1ble/result.html


Use Firefox plugin ReloadEvery_ to reload automatically HTML output during decryption.

.. _ReloadEvery: https://addons.mozilla.org/en-US/firefox/addon/reloadevery/

Some **explanations** (in French), with diagrams_.

This program was developed to solve this assignment_
for the ULB course *INFO-F405 Introduction to cryptography*.
(20/20)

.. _diagrams: https://bitbucket.org/OPiMedia/p0ss1ble/src/master/explanations/
.. _assignment: https://bitbucket.org/OPiMedia/p0ss1ble/src/master/assignment/

|



Author: 🌳 Olivier Pirson — OPi |OPi| 🇧🇪🇫🇷🇬🇧 🐧 👨‍💻 👨‍🔬
=================================================================
🌐 Website: http://www.opimedia.be/

💾 Bitbucket: https://bitbucket.org/OPiMedia/

* 📧 olivier.pirson.opi@gmail.com
* Mastodon: https://mamot.fr/@OPiMedia — Twitter: https://twitter.com/OPirson
* 👨‍💻 LinkedIn: https://www.linkedin.com/in/olivierpirson/ — CV: http://www.opimedia.be/CV/English.html
* other profiles: http://www.opimedia.be/about/

.. |OPi| image:: http://www.opimedia.be/_png/OPi.png

|



License: GPLv3_ |GPLv3|
=======================
Copyright (C) 2016, 2017 Olivier Pirson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.

.. _GPLv3: https://www.gnu.org/licenses/gpl-3.0.html

.. |GPLv3| image:: https://www.gnu.org/graphics/gplv3-88x31.png



|p0ss1ble|

.. |p0ss1ble| image:: https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2020/Jun/15/3877291624-2-p0ss1ble-logo_avatar.png
